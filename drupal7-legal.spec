%{?drupal7_find_provides_and_requires}

%global module legal

Name:          drupal7-%{module}
Version:       1.11
Release:       1%{?dist}
Summary:       Terms and conditions functionality for Drupal

License:       GPLv2+
URL:           http://drupal.org/project/%{module}
Source0:       http://ftp.drupal.org/files/projects/%{module}-7.x-%{version}.tar.gz

BuildArch:     noarch
BuildRequires: drupal7-rpmbuild >= 7.23-3

Requires:      drupal7


%description
Displays your Terms & Conditions to users who want to register, and
requires that they accept the T&C before their registration is
accepted.

If T&Cs are changed users with an existing account will be asked to
accept the new version, and will not be able to log in until they
have.

Terms & Conditions can be displayed as styled text, or in a scroll
box.

This package provides the following Drupal module:
* %{module}


%prep
%setup -qn %{module}


%build
# Empty build section, nothing to build


%install
mkdir -p %{buildroot}%{drupal7_modules}/%{module}
cp -pr * %{buildroot}%{drupal7_modules}/%{module}/


%files
%license LICENSE.txt
%doc README.txt
%{drupal7_modules}/%{module}
%exclude %{drupal7_modules}/%{module}/*.txt


%changelog
* Wed Feb 23 2022 Daniel J. R. May <daniel.may@danieljrmay.com> - 1.11-1
- Update to match latest upstream release.

* Tue Nov 27 2018 Daniel J. R. May <daniel.may@kada-media.com> - 1.10-2
- Remove Group tag from spec.

* Tue Nov 27 2018 Daniel J. R. May <daniel.may@kada-media.com> - 1.10-1
- Initial release of RPM packaging.
