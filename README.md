# drupal-legal

RPM packaging for the [Legal](https://www.drupal.org/project/legal) Drupal module.


## Copr Respository 

If you are using Fedora, RHEL or CentOS (or similar) then you can
install the `drupal7-legal` RPM via a [Copr
repository](https://copr.fedorainfracloud.org/coprs/danieljrmay/drupal-packages/).

```console
$ dnf copr enable danieljrmay/drupal-packages
$ dnf install drupal7-legal
```
