#!/usr/bin/make -f
#
# drupal-legal GNU Makefile
#
# See: https://www.gnu.org/software/make/manual
#
# Copyright (c) 2018 Daniel J. R. May
#

# Makefile command variables
CURL=/usr/bin/curl
DNF_INSTALL=/usr/bin/dnf --assumeyes install
MOCK=/usr/bin/mock
RPM=/usr/bin/rpm
RPMLINT=/usr/bin/rpmlint
SHELL=/bin/sh
YAMLLINT=/usr/bin/yamllint

# Makefile parameter variables
module_name:=legal
name:=drupal7-$(module_name)
version:=$(shell awk '/Version:/ { print $$2 }' $(name).spec)
dist_tree-ish:=7.x-$(version)
dist_name:=$(module_name)-$(dist_tree-ish)
fedora_release:=$(shell $(RPM) --query --queryformat '%{VERSION}\n' fedora-release)
fedora_dist:=.fc$(fedora_release)
fedora_rpm_stem:=$(shell $(RPM) --define='dist $(fedora_dist)' --specfile $(name).spec)
fedora_rpm:=$(fedora_rpm_stem).rpm
fedora_srpm:=$(patsubst %.noarch, %.src.rpm, $(fedora_rpm_stem))
fedora_mock_root:=default
epel_release:=7
epel_dist:=.el$(epel_release)
epel_rpm_stem:=$(shell $(RPM) --define='dist $(epel_dist)' --specfile $(name).spec)
epel_rpm:=$(epel_rpm_stem).rpm
epel_srpm:=$(patsubst %.noarch, %.src.rpm, $(epel_rpm_stem))
epel_mock_root:=epel-$(epel_release)-x86_64
mock_resultdir:=.
requirements:=curl rpm rpm-build rpmlint mock
tarball:=$(dist_name).tar.gz

.PHONY: all
all:
	$(info all:)

.PHONY: lint
lint:
	$(info lint:)
	$(RPMLINT) $(name).spec
	$(YAMLLINT) .gitlab-ci.yml

.PHONY: dist
dist:  $(tarball)

$(tarball):
	$(CURL) -O https://ftp.drupal.org/files/projects/$(tarball)

.PHONY: srpm
srpm: $(fedora_srpm) $(epel_srpm)

$(fedora_srpm): $(tarball)
	$(MOCK) --root=$(fedora_mock_root) --resultdir=$(mock_resultdir) --buildsrpm \
		--spec $(name).spec --sources $(tarball)

$(epel_srpm):
	$(MOCK) --root=$(epel_mock_root) --resultdir=$(mock_resultdir) --buildsrpm \
		--spec $(name).spec --sources $(tarball)

.PHONY: rpm
rpm: $(fedora_rpm) $(epel_rpm)

$(fedora_rpm): $(fedora_srpm)
	$(MOCK) --root=$(fedora_mock_root) --resultdir=$(mock_resultdir) --rebuild $(fedora_srpm)

$(epel_rpm): $(epel_srpm)
	$(MOCK) --root=$(epel_mock_root) --resultdir=$(mock_resultdir) --rebuild $(epel_srpm)

.PHONY: requirements
requirements:
	$(DNF_INSTALL) $(requirements)

.PHONY: clean
clean:
	$(info clean:)
	rm -f *.rpm 

.PHONY: distclean
distclean: clean
	$(info distclean:)
	rm -f *~ *.log 

.PHONY: help
help:
	$(info help:)
	$(info Usage: make TARGET [VAR1=VALUE VAR2=VALUE])
	$(info )
	$(info Targets:)
	$(info   all              The default target, builds all files.)
	$(info   lint             Lint all source files.)
	$(info   dist             Get the source distribution tarball.)
	$(info   srpm             Build the source RPM.)
	$(info   rpm              Build the RPM.)
	$(info   dist             Get the source distribution tarball.)
	$(info   distclean        Clean up all generated binary files.)
	$(info   distclean        Clean up all generated files.)
	$(info   requirements     Install all project development requirements, requires sudo.)
	$(info   help             Display this help message.)
	$(info   printvars        Print variable values (useful for debugging).)
	$(info   printmakevars    Print the Make variable values (useful for debugging).)
	$(info )
	$(info For more information read the Makefile and see http://www.gnu.org/software/make/manual/html_node/index.html)

.PHONY: printvars
printvars:
	$(info printvars:)
	$(info CURL=$(CURL))
	$(info DNF_INSTALL=$(DNF_INSTALL))
	$(info MOCK=$(MOCK))
	$(info RPM=$(RPM))
	$(info RPMLINT=$(RPMLINT))
	$(info SHELL=$(SHELL))
	$(info YAMLLINT=$(YAMLLINT))
	$(info module_name=$(module_name))
	$(info name=$(name))
	$(info version=$(version))
	$(info release=$(release))
	$(info dist_tree-ish=$(dist_tree-ish))
	$(info dist_name=$(dist_name))
	$(info fedora_release=$(fedora_release))
	$(info fedora_dist=$(fedora_dist))
	$(info fedora_rpm_stem=$(fedora_rpm_stem))
	$(info fedora_rpm=$(fedora_rpm))
	$(info fedora_srpm=$(fedora_srpm))
	$(info fedora_mock_root=$(fedora_mock_root))
	$(info epel_release=$(epel_release))
	$(info epel_dist=$(epel_dist))
	$(info epel_rpm=$(epel_rpm))
	$(info epel_srpm=$(epel_srpm))
	$(info epel_mock_root=$(epel_mock_root))
	$(info mock_resultdir=$(mock_resultdir))
	$(info requirements=$(requirements))
	$(info tarball=$(tarball))

.PHONY: printmakevars
printmakevars:
	$(info printmakevars:)
	$(info $(.VARIABLES))
